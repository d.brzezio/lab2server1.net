﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Lab1Server2
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            byte[] bytes;
            Socket socket = null;
            try
            {
                socket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Any, 11111);
                socket.Bind(iPEndPoint);
                socket.Listen(1);
                while (true)
                {
                    Console.WriteLine("ready");
                    Socket socketClient = null;
                    try
                    {
                        socketClient = socket.Accept();
                        str = null;
                        bytes = new byte[1024];
                        int data = socketClient.Receive(bytes);
                        str += Encoding.ASCII.GetString(bytes, 0, data);
                        Console.WriteLine(str);
                        socketClient.Send(Encoding.ASCII.GetBytes(str));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                        if (socketClient != null)
                            socketClient.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
               
            }
            finally
            {
                if (socket != null)
                    socket.Close();
            }

        }
    }
}
